#include <iostream>
#include <string>
#include "parser.h"
#include "parser.cpp"
#include "utility.h"
int main()
{
	std::cout<<"Enter the calculation:\n?";
	Parser theParser = Parser();
	std::string input;
	std::cin>>input;
	theParser.setText(input.c_str());
	int a,b;
	char c;
	if(isNumber(theParser.peekNextToken()))
		a = getIntFromString(theParser.nextToken());
	else return 1;
	if(isOperator(theParser.peekNextToken()))
		c = theParser.nextToken().operator[](0);
	else return 1;
	if(isNumber(theParser.peekNextToken()))
		b = getIntFromString(theParser.nextToken());
	else return 1;
	std::cout<<"\n";
	switch(c)
	{
		case 'X':
		case 'x':
		case '*':
		std::cout<< a*b;
		break;
		case '+':
		std::cout<<a+b;
		break;
		case '-':
		std::cout<<a-b;
		break;
		case '/':
		std::cout<<a/b;
		break;
		default:
		std::cout<<"wrong input";
		return 1;
	}
}
