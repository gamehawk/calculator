//#include"Parser.h"
#include <iostream>
#include "utility.h"
#include<string>
//returns the current token,then pulls a token from the string to Token,ignores space

using namespace std;

class Parser
{
	private:
		 string Token;
		 string currentText;
		int pos;

	public:
	    string peekNextToken();
		string nextToken();
		inline void setText(string);
		Parser(string);
		Parser();
};

string Parser::nextToken()
{
	if(!(&Token == NULL))
	{
		string lastToken = Token;
		if(pos>currentText.size())return NULL;
		if(currentText[pos]>='0'&&currentText[pos]<='9')
		{
			string temp(" ");
			//TODO:fix
			while((currentText.at(pos)>='1') && (currentText.at(pos)<='9'))
				{
					temp+= currentText.at(pos);
					pos++;
				}
				Token = temp;
		}
		else switch(currentText[pos])
		{
			case '*':
			case 'x':
			case '+':
			case '-':
			case '/':
			case '%':
			case '=':
				Token = currentText[pos];
				pos++;
			break;
			case ' ':
				pos++;
				nextToken();
			default:
				return NULL;
		}
		return lastToken;
	}
}
void Parser::setText(string text)
{
	currentText = text;
	pos = 0;
}
Parser::Parser(string text)
{
	setText(text);
	pos = 0;
}
Parser::Parser()
{
	pos = 0;
}

string Parser::peekNextToken()
{
	return Token;
}
